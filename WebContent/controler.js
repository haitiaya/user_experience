function callServer(url, positiveResponse, negativeResponse) {
	$.ajax({
		type : "GET",
		url : url,
		dataType : "json",
		success : positiveResponse(response),
		error : negativeResponse(error)
	});
}