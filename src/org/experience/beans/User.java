package org.experience.beans;



public class User {
	
	String date;
	int idUser;
	String jeton;
	String website;
	String nameEvent;
	int idExperience;
	
	public User(){
	}
	
	public User(String date, int idUser, String jeton, String site, String nameEvent,int idExperience){
		this.date = date;
		this.idUser = idUser;
		this.jeton = jeton;
		this.website = site;
		this.nameEvent = nameEvent;
		this.idExperience=idExperience;
	}
	
	public User(String date, int idUser){
		this.date = date;
		this.idUser = idUser;
	}

	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public int getIdUser() {
		return idUser;
	}
	
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getNameEvent() {
		return nameEvent;
	}
	public void setNameEvent(String nameEvent) {
		this.nameEvent = nameEvent;
	}
	public int getIdExperience() {
		return idExperience;
	}
	public void setIdExperience(int idExperience) {
		this.idExperience = idExperience;
	}
	public String getJeton() {
		return jeton;
	}
	public void setJeton(String jeton) {
		this.jeton = jeton;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	
	
}
