package org.experience.controlerImpl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.experience.beans.BDDinformation;
import org.experience.beans.User;
import org.experience.controler.Controler;
import org.experience.daoImpl.DaoImpl;

public class ControlerImpl implements Controler {
	
	static DaoImpl daoImplObjet=new DaoImpl();
	
	
	public  boolean maestroWriteBDD(User user){
		boolean worked = false;
		Connection C = daoImplObjet.connect(BDDinformation.DB_NAME,BDDinformation.DB_USER,BDDinformation.DB_PASSWORD,BDDinformation.numPort,BDDinformation.bdd_IP);
		try {
			daoImplObjet.writeBDD(user, C);
			worked = true;
		} catch (SQLException e) {
			closeConnection(C);
		}
		return worked;
		
	}
	
	public List<String> maestroFilter(User user){
		List<String> liste = new ArrayList<String>();
		Connection C = daoImplObjet.connect(BDDinformation.DB_NAME, BDDinformation.DB_USER, BDDinformation.DB_PASSWORD, BDDinformation.numPort, BDDinformation.bdd_IP);
		try {
			String date = user.getDate();
			int userid = user.getIdUser();
			liste = daoImplObjet.lireEnregitrement(C, date, userid);
			daoImplObjet.afficheEnregistrement(liste);
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			closeConnection(C);
		}
		return liste;
	}

	public static void afficheEnregistrement(List<String> liste){
		for(int i=0;i<liste.size();i++){
			System.out.print(liste.get(i));
		}
	}
	
	private void closeConnection(Connection C){
		try {
			C.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
