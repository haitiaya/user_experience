package org.experience.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import org.experience.beans.User;

	public interface Dao {
		
		public Connection connect(String dataBaseName, String username, String password, int numeroPort, String bdd_IP);
		public void  writeBDD(User U, Connection C) throws SQLException;
		public  List<String> lireEnregitrement(Connection connection,String date, int userid);
		
	}
