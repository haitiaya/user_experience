package org.experience.daoImpl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.experience.beans.User;

public class DaoImpl {
	
	public DaoImpl(){
	}
	
	public Connection connect(String dataBaseName, String username, String password, int numeroPort, String bdd_IP){
		System.out.println("-------- PostgreSQL "+ "JDBC Connection Testing ------------");
		Connection connection = null;
		try{
			Class.forName("org.postgresql.Driver");
		}

		catch(ClassNotFoundException e){
			System.out.println("Where is your PostgreSQL JDBC Driver? "+ "Include in your library path!");
			e.printStackTrace();
			return connection;
		}
		System.out.println("PostgreSQL JDBC Driver Registered!");
		try{
			connection = DriverManager.getConnection(
					"jdbc:postgresql://"+bdd_IP+":"+numeroPort+"/"+dataBaseName, username,
					password);
		}
		catch(SQLException e){
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
		}
		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		}
		else{
			System.out.println("Failed to make connection!");
		}
		return connection;
		
	}
	
	public  List<String> lireEnregitrement(Connection connection, String date, int userId) throws SQLException{
		List<String> liste = new ArrayList<String>();
		ResultSet m_ResultSet = null;
		String query = "";
		PreparedStatement p;

		query = "SELECT * FROM user_experience WHERE date = ? AND id_user = ?";
		//Execute the query
		p = connection.prepareStatement(query);
		p.setString(1,date);
		p.setInt(2,userId);
		m_ResultSet = p.executeQuery();
		//Loop through the results
		while (m_ResultSet.next()) {
			liste.add(m_ResultSet.getString(2)+"");
			liste.add(m_ResultSet.getInt(1)+"");
			liste.add(m_ResultSet.getString(3));
			liste.add(m_ResultSet.getString(4));
			liste.add(m_ResultSet.getString(6));
			liste.add(m_ResultSet.getInt(5)+"");

		}

		if (m_ResultSet != null) {
			m_ResultSet.close();
		}
		if (p != null) {
			p.close();
		}
		if (connection != null) {
			connection.close();
		} 

		return liste;
	}
	
	public  void  writeBDD(User U, Connection C) throws SQLException {
		Statement statement;
		String newDate = U.getDate();
		int idUser = U.getIdUser();
		String token = U.getJeton();
		String nameEvent = U.getNameEvent();
		String siteWeb = U.getWebsite();
		int experienceId = U.getIdExperience();
		String insertTableSQL = "INSERT INTO user_experience (id_user,jeton,website,name_event,id_experience,date)"
				+ "VALUES ('"+idUser+"','"+token+"','"+siteWeb+"','"+nameEvent+"','"+experienceId+"','"+newDate+"')";
		statement = C.createStatement();
		System.out.println(insertTableSQL);
		statement.executeUpdate(insertTableSQL);
		if (statement != null) {
			statement.close();
		}
		if (C != null) {
			C.close();
		}
		
	}
	
	public static void afficheEnregistrement(List<String> liste){
		for(int i=0;i<liste.size();i++){
			System.out.print(liste.get(i));
		}
	}
	
}
