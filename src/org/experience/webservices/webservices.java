package org.experience.webservices;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.experience.beans.User;
import org.experience.controlerImpl.ControlerImpl;
import org.experience.daoImpl.DaoImpl;

import com.google.gson.Gson;

@Path("/user")
public class webservices {
	@GET
	@Path("/msg")
	public Response getMsg() {
 
		String output = "Jersey say : hi";
 
		return Response.status(200).entity(output).build();
	}

	
	@GET
	@Path("/Insert/{id_user}/{jeton}/{website}/{name_event}/{id_experience}/{date}")
	public Response insertEvent(@PathParam("id_user") int idUser,
			@PathParam("jeton") String token,
			@PathParam("website") String siteweb,
			@PathParam("name_event") String nomEvenement,
			@PathParam("id_experience") int idExperience,
			@PathParam("date") String dates){
		User user = new User(dates,idUser,token,siteweb,nomEvenement,idExperience);
		ControlerImpl daoInstance = new ControlerImpl();
		boolean h = daoInstance.maestroWriteBDD(user);
		String outputRegistration = "";
		if(h){
			outputRegistration = "TRUE";
		}
		if(!h){
			outputRegistration = "FALSE";
		}
		return Response.status(200).entity(outputRegistration).build();
	}
	
	
	@GET
	@Path("/filtre/{date}/{userId}")
	@Produces("application/json")
	public Response getEvents(@PathParam("date")String date, 
											@PathParam("userId") int idUser){
		ControlerImpl daoInstance = new ControlerImpl();
		User user = new User(date,idUser);
		List<String> enregistrement = daoInstance.maestroFilter(user);
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(enregistrement)).build();
	}
	
}
